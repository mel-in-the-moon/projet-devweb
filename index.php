
<?php
   
    $variable = "comments en anglais";
    $utilisateurs = [
        ["nom"=> "Landry", "prenom" => "Melanie", "comment"=> "c'est hot les tableaux"],
        ["nom"=> "Landry", "prenom" => "Mel", "comment"=> "c'est beau les tableaux"],
        ["nom"=> "Landry", "prenom" => "Melaloo", "comment"=> "c'est compliqué les tableaux"]       
    ];

    include_once './includes/comment/form-comment.php'; 
    ?>
<section class="hero">
  <div class="hero-body">
    <div class="container is-fluid">
        <h1 class="title"><?php echo $variable; ?></h1>
    </div>
  </div>
<section>
    <div class="container is-fluid">
     
<?php
    foreach($utilisateurs as $cle => $user) {
     
?>

        <h5 class="subtitle is-5">Nom complet: <?php echo $user["prenom"]?><?php echo $user["nom"]?></h5>
        <h6 class="subtitle is-6">Commentaire:</h6>
        <section>
           <?php echo $user["comment"];?>
        </section>

<?php
    }
?>
    </div>
</section>
    <?php
    include_once './includes/parts/footer.php';
?>

